﻿using Datos.Contexto;
using Infotrack.Transaccional.EF.Clases;
using Infotrack.Utilitarios.Clases.Comunes.Entidades;
using Infotrack.Utilitarios.Clases.Mapeador.Extensiones;
using Infotrack.Utilitarios.Transversales.Clases.Comunes.Enumeraciones;
using InterfacesComunes.Acciones.Entidades;
using InterfacesComunes.App_LocalResources;
using InterfacesComunes.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Clases.DAL
{
    public class AlumnoDAL : AccesoComunDAL<ContextoNotas>, IAlumnoAccion
    {
        Respuesta<IAlumnoDTO> RespuestaAlumno;
        RepositorioGenerico<Alumno> RepositorioAlumno;

        public AlumnoDAL()
        {
            this.RespuestaAlumno = new Respuesta<IAlumnoDTO>();
            this.RepositorioAlumno = new RepositorioGenerico<Alumno>(ContextoBD);
        }
        public Respuesta<IAlumnoDTO> ConsultarListaAlumnos()
        {
            return this.EjecutarTransaccion<Respuesta<IAlumnoDTO>, AlumnoDAL>(() =>
            {
                RespuestaAlumno.Entidades = RepositorioAlumno.BuscarTodos().ToList<IAlumnoDTO>();
                RespuestaAlumno.Resultado = true;
                RespuestaAlumno.TipoNotificacion = TipoNotificacion.Exitoso;
                return RespuestaAlumno;

            });

        }

        public Respuesta<IAlumnoDTO> ConsultarListaAlumnosLLave(IAlumnoDTO alumno)
        {
            ContextoNotas contexto = FabricaContexto.Invoke();

            return this.EjecutarTransaccion<Respuesta<IAlumnoDTO>, AlumnoDAL>(() =>
            {
                RespuestaAlumno.Entidades = (from entidad in contexto.Alumno
                                       where entidad.IdAlumno == alumno.IdAlumno
                                       select entidad).ToList<IAlumnoDTO>();

                RespuestaAlumno.Resultado = true;
                return RespuestaAlumno;

            });
        }

        public Respuesta<IAlumnoDTO> ConsultarListaAlumnosPorFiltro(IAlumnoDTO alumno, Expression<Func<IAlumnoDTO, bool>> filtro)
        {
            return this.EjecutarTransaccion<Respuesta<IAlumnoDTO>, AlumnoDAL>(() =>
            {
                RespuestaAlumno.Entidades = RepositorioAlumno.BuscarPor(Mapeador.MapearExpresion<IAlumnoDTO, Alumno>(filtro)).ToList<IAlumnoDTO>();
                RespuestaAlumno.Resultado = true;
                return RespuestaAlumno;

            });
        }

        public Respuesta<IAlumnoDTO> EditarAlumno(IAlumnoDTO alumno)
        {
            return this.EjecutarTransaccion<Respuesta<IAlumnoDTO>, AlumnoDAL>(() =>
            {
                Alumno AlumnoEntidad = (RepositorioAlumno.BuscarPor(entidad => entidad.IdAlumno == alumno.IdAlumno).FirstOrDefault());
                Mapeador.MapearObjetosPorPropiedad(alumno, AlumnoEntidad);
                RepositorioAlumno.Editar(AlumnoEntidad);
                RepositorioAlumno.Guardar();
                RespuestaAlumno.Resultado = true;
                RespuestaAlumno.Mensajes.Add(MensajesComunes.MensajeCreacionEdicionExitosa);
                return RespuestaAlumno;
            });
        }

        public Respuesta<IAlumnoDTO> EditarAlumnoPorFiltro(IAlumnoDTO alumno, params string[] propiedades)
        {
            return this.EjecutarTransaccion<Respuesta<IAlumnoDTO>, AlumnoDAL>(() =>
            {
                Alumno AlumnoEntidad = (RepositorioAlumno.BuscarPor(entidad => entidad.IdAlumno == alumno.IdAlumno).FirstOrDefault());
                Mapeador.MapearObjetosPorPropiedadPorFiltro(alumno, AlumnoEntidad, propiedades);
                RepositorioAlumno.Editar(AlumnoEntidad);
                RepositorioAlumno.Guardar();
                RespuestaAlumno.Resultado = true;
                RespuestaAlumno.Mensajes.Add(MensajesComunes.MensajeCreacionEdicionExitosa);
                return RespuestaAlumno;
            });
        }

        public Respuesta<IAlumnoDTO> EliminarAlumno(IAlumnoDTO alumno)
        {
            return this.EjecutarTransaccion<Respuesta<IAlumnoDTO>, AlumnoDAL>(() =>
            {
                Alumno AlumnoEntidad = (RepositorioAlumno.BuscarPor(entidad => entidad.IdAlumno == alumno.IdAlumno).FirstOrDefault());
                RepositorioAlumno.Eliminar(AlumnoEntidad);
                RepositorioAlumno.Guardar();
                RespuestaAlumno.Resultado = true;
                RespuestaAlumno.Mensajes.Add(MensajesComunes.MensajeEntidadEliminadaConExito);
                return RespuestaAlumno;
            });
        }

        public Respuesta<IAlumnoDTO> GuardarAlumno(IAlumnoDTO alumno)
        {
            return this.EjecutarTransaccion<Respuesta<IAlumnoDTO>, AlumnoDAL>(() =>
            {
                Alumno AlumnoEntidad = Mapeador.MapearEntidadDTO(alumno, new Alumno());
                RepositorioAlumno.Agregar(AlumnoEntidad);
                RepositorioAlumno.Guardar();
                RespuestaAlumno.Resultado = true;
                RespuestaAlumno.Mensajes.Add(MensajesComunes.MensajeCreacionEdicionExitosa);
                RespuestaAlumno.Entidades.Add(AlumnoEntidad);
                RespuestaAlumno.TipoNotificacion = TipoNotificacion.Exitoso;
                return RespuestaAlumno;

            });
        }

        public Respuesta<IAlumnoDTO> GuardarListaAlumno(List<IAlumnoDTO> listaAlumno)
        {
            return this.EjecutarTransaccion<Respuesta<IAlumnoDTO>, AlumnoDAL>(() =>
            {
                List<Alumno> listaAlumnoEntidad = Mapeador.MapearALista<IAlumnoDTO, Alumno>(listaAlumno);
                RepositorioAlumno.AgregarLista(listaAlumnoEntidad);
                RespuestaAlumno.Resultado = true;
                RespuestaAlumno.Mensajes.Add(MensajesComunes.MensajeCreacionEdicionExitosa);
                RespuestaAlumno.Entidades.AddRange(listaAlumnoEntidad);
                return RespuestaAlumno;
            });
        }
    }
}
