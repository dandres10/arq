﻿using Datos.Contexto;
using Infotrack.Transaccional.EF.Clases;
using Infotrack.Utilitarios.Clases.Comunes.Entidades;
using Infotrack.Utilitarios.Clases.Mapeador.Extensiones;
using Infotrack.Utilitarios.Transversales.Clases.Comunes.Enumeraciones;
using InterfacesComunes.Acciones.Entidades;
using InterfacesComunes.App_LocalResources;
using InterfacesComunes.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Clases.DAL
{
    public class CursoDAL:AccesoComunDAL<ContextoNotas>,ICursoAccion
    {
        Respuesta<ICursoDTO> RespuestaCurso;
        RepositorioGenerico<Curso> RepositorioCurso;

        public CursoDAL()
        {
            this.RespuestaCurso = new Respuesta<ICursoDTO>();
            this.RepositorioCurso = new RepositorioGenerico<Curso>(ContextoBD);
        }

        public Respuesta<ICursoDTO> ConsultarListaCursos()
        {
            return this.EjecutarTransaccion<Respuesta<ICursoDTO>, CursoDAL>(() =>
            {
                RespuestaCurso.Entidades = RepositorioCurso.BuscarTodos().ToList<ICursoDTO>();
                RespuestaCurso.Resultado = true;
                RespuestaCurso.TipoNotificacion = TipoNotificacion.Exitoso;
                return RespuestaCurso;

            });
        }

        public Respuesta<ICursoDTO> ConsultarListaCursosLLave(ICursoDTO curso)
        {
            ContextoNotas contexto = FabricaContexto.Invoke();

            return this.EjecutarTransaccion<Respuesta<ICursoDTO>, CursoDAL>(() =>
            {
                RespuestaCurso.Entidades = (from entidad in contexto.Curso
                                             where entidad.IdCurso == curso.IdCurso
                                             select entidad).ToList<ICursoDTO>();

                RespuestaCurso.Resultado = true;
                return RespuestaCurso;

            });
        }

        public Respuesta<ICursoDTO> ConsultarListaCursosPorFiltro(ICursoDTO curso, Expression<Func<ICursoDTO, bool>> filtro)
        {
            return this.EjecutarTransaccion<Respuesta<ICursoDTO>, CursoDAL>(() =>
            {
                RespuestaCurso.Entidades = RepositorioCurso.BuscarPor(Mapeador.MapearExpresion<ICursoDTO, Curso>(filtro)).ToList<ICursoDTO>();
                RespuestaCurso.Resultado = true;
                return RespuestaCurso;

            });
        }

        public Respuesta<ICursoDTO> EditarCurso(ICursoDTO curso)
        {
            return this.EjecutarTransaccion<Respuesta<ICursoDTO>, CursoDAL>(() =>
            {
                Curso CursoEntidad = (RepositorioCurso.BuscarPor(entidad => entidad.IdCurso == curso.IdCurso).FirstOrDefault());
                Mapeador.MapearObjetosPorPropiedad(curso, CursoEntidad);
                RepositorioCurso.Editar(CursoEntidad);
                RepositorioCurso.Guardar();
                RespuestaCurso.Resultado = true;
                RespuestaCurso.Mensajes.Add(MensajesComunes.MensajeCreacionEdicionExitosa);
                return RespuestaCurso;
            });
        }

        public Respuesta<ICursoDTO> EditarCursoPorFiltro(ICursoDTO curso, params string[] propiedades)
        {
            return this.EjecutarTransaccion<Respuesta<ICursoDTO>, CursoDAL>(() =>
            {
                Curso CursoEntidad = (RepositorioCurso.BuscarPor(entidad => entidad.IdCurso == curso.IdCurso).FirstOrDefault());
                Mapeador.MapearObjetosPorPropiedadPorFiltro(curso, CursoEntidad, propiedades);
                RepositorioCurso.Editar(CursoEntidad);
                RepositorioCurso.Guardar();
                RespuestaCurso.Resultado = true;
                RespuestaCurso.Mensajes.Add(MensajesComunes.MensajeCreacionEdicionExitosa);
                return RespuestaCurso;
            });
        }

        public Respuesta<ICursoDTO> EliminarCurso(ICursoDTO curso)
        {
            return this.EjecutarTransaccion<Respuesta<ICursoDTO>, CursoDAL>(() =>
            {
                Curso CursoEntidad = (RepositorioCurso.BuscarPor(entidad => entidad.IdCurso == curso.IdCurso).FirstOrDefault());
                RepositorioCurso.Eliminar(CursoEntidad);
                RepositorioCurso.Guardar();
                RespuestaCurso.Resultado = true;
                RespuestaCurso.Mensajes.Add(MensajesComunes.MensajeEntidadEliminadaConExito);
                return RespuestaCurso;
            });
        }

        public Respuesta<ICursoDTO> GuardarCurso(ICursoDTO curso)
        {
            return this.EjecutarTransaccion<Respuesta<ICursoDTO>, CursoDAL>(() =>
            {
                Curso CursoEntidad = Mapeador.MapearEntidadDTO(curso, new Curso());
                RepositorioCurso.Agregar(CursoEntidad);
                RepositorioCurso.Guardar();
                RespuestaCurso.Resultado = true;
                RespuestaCurso.Mensajes.Add(MensajesComunes.MensajeCreacionEdicionExitosa);
                RespuestaCurso.Entidades.Add(CursoEntidad);
                RespuestaCurso.TipoNotificacion = TipoNotificacion.Exitoso;
                return RespuestaCurso;

            });
        }

        public Respuesta<ICursoDTO> GuardarListaCurso(List<ICursoDTO> listaCurso)
        {

            return this.EjecutarTransaccion<Respuesta<ICursoDTO>, CursoDAL>(() =>
            {
                List<Curso> listaCursoEntidad = Mapeador.MapearALista<ICursoDTO, Curso>(listaCurso);
                RepositorioCurso.AgregarLista(listaCursoEntidad);
                RespuestaCurso.Resultado = true;
                RespuestaCurso.Mensajes.Add(MensajesComunes.MensajeCreacionEdicionExitosa);
                RespuestaCurso.Entidades.AddRange(listaCursoEntidad);
                return RespuestaCurso;
            });
        }
    }
}
