﻿using Datos.Contexto;
using Infotrack.Transaccional.EF.Clases;
using Infotrack.Utilitarios.Clases.Comunes.Entidades;
using Infotrack.Utilitarios.Clases.Mapeador.Extensiones;
using Infotrack.Utilitarios.Transversales.Clases.Comunes.Enumeraciones;
using InterfacesComunes.Acciones.Entidades;
using InterfacesComunes.App_LocalResources;
using InterfacesComunes.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Clases.DAL
{
    public class CursoMateriaDAL : AccesoComunDAL<ContextoNotas>, ICursoMateriaAccion
    {
        Respuesta<ICursoMateriaDTO> RespuestaCursoMateria;
        RepositorioGenerico<CursoMateria> RepositorioCursoMateria;
        int repo;

        public CursoMateriaDAL()
        {
            this.RespuestaCursoMateria = new Respuesta<ICursoMateriaDTO>();
            this.RepositorioCursoMateria = new RepositorioGenerico<CursoMateria>(ContextoBD);
        }
        public Respuesta<ICursoMateriaDTO> ConsultarListaCursoMaterias()
        {
            return this.EjecutarTransaccion<Respuesta<ICursoMateriaDTO>, CursoMateriaDAL>(() =>
            {
                RespuestaCursoMateria.Entidades = RepositorioCursoMateria.BuscarTodos().ToList<ICursoMateriaDTO>();
                RespuestaCursoMateria.Resultado = true;
                RespuestaCursoMateria.TipoNotificacion = TipoNotificacion.Exitoso;
                return RespuestaCursoMateria;

            });

        }

        public Respuesta<ICursoMateriaDTO> ConsultarListaCursoMateriasLLave(ICursoMateriaDTO cursoMateria)
        {
            ContextoNotas contexto = FabricaContexto.Invoke();

            return this.EjecutarTransaccion<Respuesta<ICursoMateriaDTO>, CursoMateriaDAL>(() =>
            {
                RespuestaCursoMateria.Entidades = (from entidad in contexto.CursoMateria
                                                   where entidad.IdCursoMateria == cursoMateria.IdCursoMateria
                                                   select entidad).ToList<ICursoMateriaDTO>();

                RespuestaCursoMateria.Resultado = true;
                return RespuestaCursoMateria;

            });
        }

        public Respuesta<ICursoMateriaDTO> ConsultarListaCursoMateriasPorFiltro(ICursoMateriaDTO cursoMateria, Expression<Func<ICursoMateriaDTO, bool>> filtro)
        {
            return this.EjecutarTransaccion<Respuesta<ICursoMateriaDTO>, CursoMateriaDAL>(() =>
            {
                RespuestaCursoMateria.Entidades = RepositorioCursoMateria.BuscarPor(Mapeador.MapearExpresion<ICursoMateriaDTO, CursoMateria>(filtro)).ToList<ICursoMateriaDTO>();
                RespuestaCursoMateria.Resultado = true;
                return RespuestaCursoMateria;

            });
        }

        public Respuesta<ICursoMateriaDTO> EditarCursoMateria(ICursoMateriaDTO cursoMateria)
        {
            return this.EjecutarTransaccion<Respuesta<ICursoMateriaDTO>, CursoMateriaDAL>(() =>
            {
                CursoMateria CursoMateriaEntidad = (RepositorioCursoMateria.BuscarPor(entidad => entidad.IdCursoMateria == cursoMateria.IdCursoMateria).FirstOrDefault());
                Mapeador.MapearObjetosPorPropiedad(cursoMateria, CursoMateriaEntidad);
                RepositorioCursoMateria.Editar(CursoMateriaEntidad);
                RepositorioCursoMateria.Guardar();
                RespuestaCursoMateria.Resultado = true;
                RespuestaCursoMateria.Mensajes.Add(MensajesComunes.MensajeCreacionEdicionExitosa);
                return RespuestaCursoMateria;
            });
        }

        public Respuesta<ICursoMateriaDTO> EditarCursoMateriaPorFiltro(ICursoMateriaDTO cursoMateria, params string[] propiedades)
        {
            return this.EjecutarTransaccion<Respuesta<ICursoMateriaDTO>, CursoMateriaDAL>(() =>
            {
                CursoMateria CursoMateriaEntidad = (RepositorioCursoMateria.BuscarPor(entidad => entidad.IdCursoMateria == cursoMateria.IdCursoMateria).FirstOrDefault());
                Mapeador.MapearObjetosPorPropiedadPorFiltro(cursoMateria, CursoMateriaEntidad, propiedades);
                RepositorioCursoMateria.Editar(CursoMateriaEntidad);
                RepositorioCursoMateria.Guardar();
                RespuestaCursoMateria.Resultado = true;
                RespuestaCursoMateria.Mensajes.Add(MensajesComunes.MensajeCreacionEdicionExitosa);
                return RespuestaCursoMateria;
            });
        }

        public Respuesta<ICursoMateriaDTO> EliminarCursoMateria(ICursoMateriaDTO cursoMateria)
        {
            return this.EjecutarTransaccion<Respuesta<ICursoMateriaDTO>, CursoMateriaDAL>(() =>
            {
                CursoMateria CursoMateriaEntidad = (RepositorioCursoMateria.BuscarPor(entidad => entidad.IdCursoMateria == cursoMateria.IdCursoMateria).FirstOrDefault());
                RepositorioCursoMateria.Eliminar(CursoMateriaEntidad);
                RepositorioCursoMateria.Guardar();
                RespuestaCursoMateria.Resultado = true;
                RespuestaCursoMateria.Mensajes.Add(MensajesComunes.MensajeEntidadEliminadaConExito);
                return RespuestaCursoMateria;
            });
        }

        public Respuesta<ICursoMateriaDTO> GuardarCursoMateria(ICursoMateriaDTO cursoMateria)
        {
            return this.EjecutarTransaccion<Respuesta<ICursoMateriaDTO>, CursoMateriaDAL>(() =>
            {
                CursoMateria CursoMateriaEntidad = Mapeador.MapearEntidadDTO(cursoMateria, new CursoMateria());
                RepositorioCursoMateria.Agregar(CursoMateriaEntidad);
                RepositorioCursoMateria.Guardar();
                RespuestaCursoMateria.Resultado = true;
                RespuestaCursoMateria.Mensajes.Add(MensajesComunes.MensajeCreacionEdicionExitosa);
                RespuestaCursoMateria.Entidades.Add(CursoMateriaEntidad);
                RespuestaCursoMateria.TipoNotificacion = TipoNotificacion.Exitoso;
                return RespuestaCursoMateria;

            });
        }

        public Respuesta<ICursoMateriaDTO> GuardarListaCursoMateria(List<ICursoMateriaDTO> listaCursoMateria)
        {
            return this.EjecutarTransaccion<Respuesta<ICursoMateriaDTO>, CursoMateriaDAL>(() =>
            {
                List<CursoMateria> listaCursoMateriaEntidad = Mapeador.MapearALista<ICursoMateriaDTO, CursoMateria>(listaCursoMateria);
                RepositorioCursoMateria.AgregarLista(listaCursoMateriaEntidad);
                RespuestaCursoMateria.Resultado = true;
                RespuestaCursoMateria.Mensajes.Add(MensajesComunes.MensajeCreacionEdicionExitosa);
                RespuestaCursoMateria.Entidades.AddRange(listaCursoMateriaEntidad);
                return RespuestaCursoMateria;
            });
        }
    }
}
