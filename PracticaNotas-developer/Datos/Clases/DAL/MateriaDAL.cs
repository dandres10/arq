﻿using Datos.Clases.DO;
using Datos.Contexto;
using Infotrack.Transaccional.EF.Clases;
using Infotrack.Utilitarios.Clases.Comunes.Entidades;
using Infotrack.Utilitarios.Clases.Mapeador.Extensiones;
using Infotrack.Utilitarios.Transversales.Clases.Comunes.Enumeraciones;
using InterfacesComunes.Acciones.Entidades;
using InterfacesComunes.App_LocalResources;
using InterfacesComunes.DTO;
using InterfacesComunes.DTO.EntidadesConsulta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace Datos.Clases.DAL
{
    public class MateriaDAL : AccesoComunDAL<ContextoNotas>, IMateriaAccion
    {
        Respuesta<IMateriaDTO> RespuestaMateria;
        RepositorioGenerico<Materia> RepositorioMateria;

        public MateriaDAL()
        {
            this.RespuestaMateria = new Respuesta<IMateriaDTO>();
            this.RepositorioMateria = new RepositorioGenerico<Materia>(ContextoBD);
        }
        public Respuesta<IMateriaDTO> ConsultarListaMaterias()
        {
            return this.EjecutarTransaccion<Respuesta<IMateriaDTO>, MateriaDAL>(() =>
            {
                RespuestaMateria.Entidades = RepositorioMateria.BuscarTodos().ToList<IMateriaDTO>();
                RespuestaMateria.Resultado = true;
                RespuestaMateria.TipoNotificacion = TipoNotificacion.Exitoso;
                return RespuestaMateria;

            });

        }

        public Respuesta<IMateriaDTO> ConsultarListaMateriasLLave(IMateriaDTO materia)
        {
            ContextoNotas contexto = FabricaContexto.Invoke();

            return this.EjecutarTransaccion<Respuesta<IMateriaDTO>, MateriaDAL>(() =>
            {
                RespuestaMateria.Entidades = (from entidad in contexto.Materia
                                             where entidad.IdMateria == materia.IdMateria
                                             select entidad).ToList<IMateriaDTO>();

                RespuestaMateria.Resultado = true;
                return RespuestaMateria;

            });
        }

        public Respuesta<IMateriaDTO> ConsultarListaMateriasPorFiltro(IMateriaDTO materia, Expression<Func<IMateriaDTO, bool>> filtro)
        {
            return this.EjecutarTransaccion<Respuesta<IMateriaDTO>, MateriaDAL>(() =>
            {
                RespuestaMateria.Entidades = RepositorioMateria.BuscarPor(Mapeador.MapearExpresion<IMateriaDTO, Materia>(filtro)).ToList<IMateriaDTO>();
                RespuestaMateria.Resultado = true;
                return RespuestaMateria;

            });
        }

        public Respuesta<IMateriaDTO> EditarMateria(IMateriaDTO materia)
        {
            return this.EjecutarTransaccion<Respuesta<IMateriaDTO>, MateriaDAL>(() =>
            {
                Materia MateriaEntidad = (RepositorioMateria.BuscarPor(entidad => entidad.IdMateria == materia.IdMateria).FirstOrDefault());
                Mapeador.MapearObjetosPorPropiedad(materia, MateriaEntidad);

                RepositorioMateria.Editar(MateriaEntidad);
                RepositorioMateria.Guardar();
                RespuestaMateria.Resultado = true;
                RespuestaMateria.Mensajes.Add(MensajesComunes.MensajeCreacionEdicionExitosa);
                return RespuestaMateria;
            });
        }

        public Respuesta<IMateriaDTO> EditarMateriaPorFiltro(IMateriaDTO materia, params string[] propiedades)
        {
            return this.EjecutarTransaccion<Respuesta<IMateriaDTO>, MateriaDAL>(() =>
            {
                Materia MateriaEntidad = (RepositorioMateria.BuscarPor(entidad => entidad.IdMateria == materia.IdMateria).FirstOrDefault());
                Mapeador.MapearObjetosPorPropiedadPorFiltro(materia, MateriaEntidad, propiedades);
                RepositorioMateria.Editar(MateriaEntidad);
                RepositorioMateria.Guardar();
                RespuestaMateria.Resultado = true;
                RespuestaMateria.Mensajes.Add(MensajesComunes.MensajeCreacionEdicionExitosa);
                return RespuestaMateria;
            });
        }

        public Respuesta<IMateriaDTO> EliminarMateria(IMateriaDTO materia)
        {
            return this.EjecutarTransaccion<Respuesta<IMateriaDTO>, MateriaDAL>(() =>
            {
                Materia MateriaEntidad = (RepositorioMateria.BuscarPor(entidad => entidad.IdMateria == materia.IdMateria).FirstOrDefault());
                RepositorioMateria.Eliminar(MateriaEntidad);
                RepositorioMateria.Guardar();
                RespuestaMateria.Resultado = true;
                RespuestaMateria.Mensajes.Add(MensajesComunes.MensajeEntidadEliminadaConExito);
                return RespuestaMateria;
            });
        }

        public Respuesta<IMateriaDTO> GuardarMateria(IMateriaDTO materia)
        {
            return this.EjecutarTransaccion<Respuesta<IMateriaDTO>, MateriaDAL>(() =>
            {
                Materia MateriaEntidad = Mapeador.MapearEntidadDTO(materia, new Materia());
                RepositorioMateria.Agregar(MateriaEntidad);
                RepositorioMateria.Guardar();
                RespuestaMateria.Resultado = true;
                RespuestaMateria.Mensajes.Add(MensajesComunes.MensajeCreacionEdicionExitosa);
                RespuestaMateria.Entidades.Add(MateriaEntidad);
                RespuestaMateria.TipoNotificacion = TipoNotificacion.Exitoso;
                return RespuestaMateria;

            });
        }

        public Respuesta<IMateriaDTO> GuardarListaMateria(List<IMateriaDTO> listaMateria)
        {
            return this.EjecutarTransaccion<Respuesta<IMateriaDTO>, MateriaDAL>(() =>
            {
                List<Materia> listaMateriaEntidad = Mapeador.MapearALista<IMateriaDTO, Materia>(listaMateria);
                RepositorioMateria.AgregarLista(listaMateriaEntidad);
                RespuestaMateria.Resultado = true;
                RespuestaMateria.Mensajes.Add(MensajesComunes.MensajeCreacionEdicionExitosa);
                RespuestaMateria.Entidades.AddRange(listaMateriaEntidad);
                return RespuestaMateria;
            });
        }

        public Respuesta<IFiltrarMateriasCantidadAlumnosDTO> FiltrarMateriasCantidadAlumnos()
        {
            Respuesta<IFiltrarMateriasCantidadAlumnosDTO> RespuestaMateria = new Respuesta<IFiltrarMateriasCantidadAlumnosDTO>();
            return this.EjecutarTransaccion<Respuesta<IFiltrarMateriasCantidadAlumnosDTO>, MateriaDAL>(() =>
            {
            RespuestaMateria.Entidades = (from ma in ContextoBD.Materia
                                          join cursoM in ContextoBD.CursoMateria on
                                          ma.IdMateria equals cursoM.IdCursoMateria
                                          join nota in ContextoBD.Nota on
                                          cursoM.IdCursoMateria equals nota.IdNota
                                          join alumno in ContextoBD.Alumno on
                                          nota.IdNota equals alumno.IdAlumno
                                         into NombreCantidadAlumnos
                                          select new FiltraMateriaCantidadAlumnos
                                          {
                                               Materia =  ma.Nombre,
                                               CantidadEstudiantes = NombreCantidadAlumnos.Where(ma.IdMateria==cursoM.IdMateria&&nota.IdCursoMateria==cursoM.IdCursoMateria&& NombreCantidadAlumnos==).Count()

                                          }



                ).ToList<IFiltrarMateriasCantidadAlumnosDTO>();
                RespuestaMateria.Resultado = true;
                RespuestaMateria.TipoNotificacion = TipoNotificacion.Exitoso;
                return RespuestaMateria;
                
            });
        }
    }
}
