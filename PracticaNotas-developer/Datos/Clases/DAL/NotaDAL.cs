﻿
namespace Datos.Clases.DAL
{

    using Datos.Clases.DO;
    using Datos.Contexto;
    using Infotrack.Transaccional.EF.Clases;
    using Infotrack.Utilitarios.Clases.Comunes.Entidades;
    using Infotrack.Utilitarios.Clases.Mapeador.Extensiones;
    using Infotrack.Utilitarios.Transversales.Clases.Comunes.Enumeraciones;
    using InterfacesComunes.Acciones.Entidades;
    using InterfacesComunes.App_LocalResources;
    using InterfacesComunes.DTO;
    using InterfacesComunes.DTO.EntidadesConsulta;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;



    public class NotaDAL : AccesoComunDAL<ContextoNotas>, INotaAccion

    {

        Respuesta<INotaDTO> RespuestaNota;
        RepositorioGenerico<Nota> RepositorioNota;

        public NotaDAL()
        {
            this.RespuestaNota = new Respuesta<INotaDTO>();
            this.RepositorioNota = new RepositorioGenerico<Nota>(ContextoBD);
        }
        public Respuesta<INotaDTO> ConsultarListaNotas()
        {
            return this.EjecutarTransaccion<Respuesta<INotaDTO>, NotaDAL>(() =>
            {
                RespuestaNota.Entidades = RepositorioNota.BuscarTodos().ToList<INotaDTO>();
                RespuestaNota.Resultado = true;
                RespuestaNota.TipoNotificacion = TipoNotificacion.Exitoso;
                return RespuestaNota;

            });

        }

        public Respuesta<INotaDTO> ConsultarListaNotasLLave(INotaDTO nota)
        {
            ContextoNotas contexto = FabricaContexto.Invoke();

            return this.EjecutarTransaccion<Respuesta<INotaDTO>, NotaDAL>(() =>
            {
                RespuestaNota.Entidades = (from entidad in contexto.Nota
                                           where entidad.IdNota == nota.IdNota
                                           select entidad).ToList<INotaDTO>();
                RespuestaNota.TipoNotificacion = TipoNotificacion.Exitoso;


                RespuestaNota.Resultado = true;
                return RespuestaNota;

            });
        }

        public Respuesta<INotaDTO> ConsultarListaNotasPorFiltro(INotaDTO nota, Expression<Func<INotaDTO, bool>> filtro)
        {
            return this.EjecutarTransaccion<Respuesta<INotaDTO>, NotaDAL>(() =>
            {
                RespuestaNota.Entidades = RepositorioNota.BuscarPor(Mapeador.MapearExpresion<INotaDTO, Nota>(filtro)).ToList<INotaDTO>();
                RespuestaNota.TipoNotificacion = TipoNotificacion.Exitoso;
                RespuestaNota.Resultado = true;
                return RespuestaNota;

            });
        }

        public Respuesta<INotaDTO> EditarNota(INotaDTO nota)
        {
            return this.EjecutarTransaccion<Respuesta<INotaDTO>, NotaDAL>(() =>
            {
                Nota NotaEntidad = (RepositorioNota.BuscarPor(entidad => entidad.IdNota == nota.IdNota).FirstOrDefault());
                Mapeador.MapearObjetosPorPropiedad(nota, NotaEntidad);
                RepositorioNota.Editar(NotaEntidad);
                RepositorioNota.Guardar();
                RespuestaNota.Resultado = true;
                RespuestaNota.TipoNotificacion = TipoNotificacion.Exitoso;
                RespuestaNota.Mensajes.Add(MensajesComunes.MensajeCreacionEdicionExitosa);
                return RespuestaNota;
            });
        }

        public Respuesta<INotaDTO> EditarNotaPorFiltro(INotaDTO nota, params string[] propiedades)
        {
            return this.EjecutarTransaccion<Respuesta<INotaDTO>, NotaDAL>(() =>
            {
                Nota NotaEntidad = (RepositorioNota.BuscarPor(entidad => entidad.IdNota == nota.IdNota).FirstOrDefault());
                Mapeador.MapearObjetosPorPropiedadPorFiltro(nota, NotaEntidad, propiedades);
                RepositorioNota.Editar(NotaEntidad);
                RepositorioNota.Guardar();
                RespuestaNota.Resultado = true;
                RespuestaNota.Mensajes.Add(MensajesComunes.MensajeCreacionEdicionExitosa);
                return RespuestaNota;
            });
        }

        public Respuesta<INotaDTO> EliminarNota(INotaDTO nota)
        {
            return this.EjecutarTransaccion<Respuesta<INotaDTO>, NotaDAL>(() =>
            {
                Nota NotaEntidad = (RepositorioNota.BuscarPor(entidad => entidad.IdNota == nota.IdNota).FirstOrDefault());
                RepositorioNota.Eliminar(NotaEntidad);
                RepositorioNota.Guardar();
                RespuestaNota.Resultado = true;
                RespuestaNota.TipoNotificacion = TipoNotificacion.Exitoso;
                RespuestaNota.Mensajes.Add(MensajesComunes.MensajeEntidadEliminadaConExito);
                return RespuestaNota;
            });
        }

        public Respuesta<INotaDTO> GuardarNota(INotaDTO nota)
        {
            return this.EjecutarTransaccion<Respuesta<INotaDTO>, NotaDAL>(() =>
            {
                Nota NotaEntidad = Mapeador.MapearEntidadDTO(nota, new Nota());
                RepositorioNota.Agregar(NotaEntidad);
                RepositorioNota.Guardar();
                RespuestaNota.Resultado = true;
                RespuestaNota.Mensajes.Add(MensajesComunes.MensajeCreacionEdicionExitosa);
                RespuestaNota.Entidades.Add(NotaEntidad);
                RespuestaNota.TipoNotificacion = TipoNotificacion.Exitoso;
                return RespuestaNota;

            });
        }

        public Respuesta<INotaDTO> GuardarListaNota(List<INotaDTO> listaNota)
        {
            return this.EjecutarTransaccion<Respuesta<INotaDTO>, NotaDAL>(() =>
            {
                List<Nota> listaNotaEntidad = Mapeador.MapearALista<INotaDTO, Nota>(listaNota);
                RepositorioNota.AgregarLista(listaNotaEntidad);
                RespuestaNota.Resultado = true;
                RespuestaNota.Mensajes.Add(MensajesComunes.MensajeCreacionEdicionExitosa);
                RespuestaNota.TipoNotificacion = TipoNotificacion.Exitoso;
                RespuestaNota.Entidades.AddRange(listaNotaEntidad);
                return RespuestaNota;
            });
        }

        public Respuesta<IConsultaNotasDTO> ConsultarReporteNotas(IConsultaNotasDTO filtro)
        {
            Respuesta<IConsultaNotasDTO> respuestaConsulta = new Respuesta<IConsultaNotasDTO>();
            return this.EjecutarTransaccion<Respuesta<IConsultaNotasDTO>, NotaDAL>(() =>
            {

                ContextoBD.Database.Log = ((mensaje) =>
                {
                    Debug.WriteLine(mensaje);
                });
                List<IConsultaNotasDTO> consultaAlumnoMateriaNota = (from a in ContextoBD.Alumno
                                                                     join n in ContextoBD.Nota
                                                                     on a.IdAlumno equals n.IdAlumno
                                                                     join cm in ContextoBD.CursoMateria
                                                                     on n.IdCursoMateria equals cm.IdCursoMateria
                                                                     join cu in ContextoBD.Curso
                                                                     on cm.IdCurso equals cu.IdCurso
                                                                     join m in ContextoBD.Materia
                                                                     on cm.IdMateria equals m.IdMateria
                                                                     where (filtro.idAlumno > 0 ? a.IdAlumno == filtro.idAlumno : true)
                                                                     where (filtro.idCurso > 0 ? cu.IdCurso == filtro.idCurso : true)
                                                                     where (filtro.idMateria > 0 ? m.IdMateria == filtro.idMateria : true)
                                                                     where (string.IsNullOrEmpty(filtro.Materia) ? true : m.Nombre.Contains(filtro.Materia))
                                                                     where ((string.IsNullOrEmpty(filtro.Nombre) ? true : filtro.Nombre.Contains(a.Nombre) || a.Nombre.Contains(filtro.Nombre)) )||
                                                                           ((string.IsNullOrEmpty(filtro.Nombre) ? true : filtro.Nombre.Contains(a.Apellido) || a.Apellido.Contains(filtro.Nombre)))
                                                                     where (string.IsNullOrEmpty(filtro.Curso) ? true : cu.Nombre.Contains(filtro.Curso))
                                                                     select new ConsultaNotasDO
                                                                     {
                                                                         idAlumno = a.IdAlumno,
                                                                         idNota = n.IdNota,
                                                                         idCurso = cu.IdCurso,
                                                                         idMateria = m.IdMateria,
                                                                         idCursoMateria = cm.IdCursoMateria,
                                                                         Nombre = a.Nombre + " " + a.Apellido,
                                                                         Curso = cu.Nombre,
                                                                         Materia = m.Nombre,
                                                                         Nota = n.Nota1
                                                                     }).ToList<IConsultaNotasDTO>()
                                                ;

                respuestaConsulta.Entidades = consultaAlumnoMateriaNota;
                respuestaConsulta.TipoNotificacion = TipoNotificacion.Exitoso;
                respuestaConsulta.Mensajes.Add(MensajesComunes.ConsultaExitosa);
                return respuestaConsulta;
            });
        }





    }
}

