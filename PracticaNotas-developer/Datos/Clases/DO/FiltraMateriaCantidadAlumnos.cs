﻿using InterfacesComunes.DTO.EntidadesConsulta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Clases.DO
{
    public class FiltraMateriaCantidadAlumnos : IFiltrarMateriasCantidadAlumnosDTO
    {
        public string Materia { get ; set ; }
        public int CantidadEstudiantes { get ; set ; }
    }
}
