namespace Datos.Contexto
{
    using System.Data.Entity;

    public partial class ContextoNotas : DbContext
    {

        public virtual DbSet<Alumno> Alumno { get; set; }
        public virtual DbSet<Curso> Curso { get; set; }
        public virtual DbSet<CursoMateria> CursoMateria { get; set; }
        public virtual DbSet<Materia> Materia { get; set; }
        public virtual DbSet<Nota> Nota { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Curso>()
                .HasMany(e => e.CursoMateria)
                .WithRequired(e => e.Curso)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Materia>()
                .HasMany(e => e.CursoMateria)
                .WithRequired(e => e.Materia)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Nota>()
                .Property(e => e.Nota1)
                .HasPrecision(5, 2);
        }
    }
}