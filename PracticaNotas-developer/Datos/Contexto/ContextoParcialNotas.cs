﻿namespace Datos.Contexto
{
    public partial class ContextoNotas
    {
        public ContextoNotas()
           : base("name=ContextoNotas")
        {
            this.Configuration.LazyLoadingEnabled = true;
            this.Configuration.ProxyCreationEnabled = false;
        }
    }
}