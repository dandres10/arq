namespace Datos.Contexto
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("CursoMateria")]
    public partial class CursoMateria
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CursoMateria()
        {
            Nota = new HashSet<Nota>();
        }

        [Key]
        public int IdCursoMateria { get; set; }

        public short Estado { get; set; }

        public int IdMateria { get; set; }

        public int IdCurso { get; set; }

        public virtual Curso Curso { get; set; }

        public virtual Materia Materia { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Nota> Nota { get; set; }
    }
}