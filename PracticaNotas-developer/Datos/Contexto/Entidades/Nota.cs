namespace Datos.Contexto
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Nota")]
    public partial class Nota
    {
        [Key]
        public int IdNota { get; set; }

        public int? IdCursoMateria { get; set; }

        public int? IdAlumno { get; set; }

        [Column("Nota")]
        public decimal? Nota1 { get; set; }

        public virtual Alumno Alumno { get; set; }

        public virtual CursoMateria CursoMateria { get; set; }
    }
}