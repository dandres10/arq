﻿using Infotrack.Utilitarios.Clases.Comunes.Entidades;
using InterfacesComunes.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesComunes.Acciones.Entidades
{
    public interface IAlumnoAccion
    {
        Respuesta<IAlumnoDTO> GuardarAlumno(IAlumnoDTO alumno);
        Respuesta<IAlumnoDTO> GuardarListaAlumno(List<IAlumnoDTO> listaAlumno);
        Respuesta<IAlumnoDTO> EditarAlumno(IAlumnoDTO alumno);
        Respuesta<IAlumnoDTO> EditarAlumnoPorFiltro(IAlumnoDTO alumno, params string[] propiedades);
        Respuesta<IAlumnoDTO> ConsultarListaAlumnos();
        Respuesta<IAlumnoDTO> ConsultarListaAlumnosLLave(IAlumnoDTO alumno);
        Respuesta<IAlumnoDTO> ConsultarListaAlumnosPorFiltro(IAlumnoDTO alumno, Expression<Func<IAlumnoDTO,bool>> filtro);
        Respuesta<IAlumnoDTO> EliminarAlumno(IAlumnoDTO alumno);




    }
}
