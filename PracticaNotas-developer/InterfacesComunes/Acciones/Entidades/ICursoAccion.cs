﻿using Infotrack.Utilitarios.Clases.Comunes.Entidades;
using InterfacesComunes.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesComunes.Acciones.Entidades
{
    public interface ICursoAccion
    {
        Respuesta<ICursoDTO> GuardarCurso(ICursoDTO curso);
        Respuesta<ICursoDTO> GuardarListaCurso(List<ICursoDTO> listaCurso);
        Respuesta<ICursoDTO> EditarCurso(ICursoDTO curso);
        Respuesta<ICursoDTO> EditarCursoPorFiltro(ICursoDTO Curso, params string[] propiedades);
        Respuesta<ICursoDTO> ConsultarListaCursos();
        Respuesta<ICursoDTO> ConsultarListaCursosLLave(ICursoDTO curso);
        Respuesta<ICursoDTO> ConsultarListaCursosPorFiltro(ICursoDTO curso, Expression<Func<ICursoDTO, bool>> filtro);
        Respuesta<ICursoDTO> EliminarCurso(ICursoDTO curso);
    }
}
