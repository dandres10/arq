﻿using Infotrack.Utilitarios.Clases.Comunes.Entidades;
using InterfacesComunes.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesComunes.Acciones.Entidades
{
    public interface ICursoMateriaAccion
    {

        Respuesta<ICursoMateriaDTO> GuardarCursoMateria(ICursoMateriaDTO cursoMateria);
        Respuesta<ICursoMateriaDTO> GuardarListaCursoMateria(List<ICursoMateriaDTO> listaCursoMateria);
        Respuesta<ICursoMateriaDTO> EditarCursoMateria(ICursoMateriaDTO cursoMateria);
        Respuesta<ICursoMateriaDTO> EditarCursoMateriaPorFiltro(ICursoMateriaDTO cursoMateria, params string[] propiedades);
        Respuesta<ICursoMateriaDTO> ConsultarListaCursoMaterias();
        Respuesta<ICursoMateriaDTO> ConsultarListaCursoMateriasLLave(ICursoMateriaDTO cursoMateria);
        Respuesta<ICursoMateriaDTO> ConsultarListaCursoMateriasPorFiltro(ICursoMateriaDTO cursoMateria, Expression<Func<ICursoMateriaDTO, bool>> filtro);
        Respuesta<ICursoMateriaDTO> EliminarCursoMateria(ICursoMateriaDTO cursoMateria);
    }
}
