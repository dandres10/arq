﻿using Infotrack.Utilitarios.Clases.Comunes.Entidades;
using InterfacesComunes.DTO;
using InterfacesComunes.DTO.EntidadesConsulta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesComunes.Acciones.Entidades
{
    public interface IMateriaAccion
    {
        Respuesta<IMateriaDTO> GuardarMateria(IMateriaDTO materia);
        Respuesta<IMateriaDTO> GuardarListaMateria(List<IMateriaDTO> listaMateria);
        Respuesta<IMateriaDTO> EditarMateria(IMateriaDTO materia);
        Respuesta<IMateriaDTO> EditarMateriaPorFiltro(IMateriaDTO materia, params string[] propiedades);
        Respuesta<IMateriaDTO> ConsultarListaMaterias();
        Respuesta<IMateriaDTO> ConsultarListaMateriasLLave(IMateriaDTO materia);
        Respuesta<IMateriaDTO> ConsultarListaMateriasPorFiltro(IMateriaDTO materia, Expression<Func<IMateriaDTO, bool>> filtro);
        Respuesta<IMateriaDTO> EliminarMateria(IMateriaDTO materia);
        Respuesta<IFiltrarMateriasCantidadAlumnosDTO> FiltrarMateriasCantidadAlumnos();
    }
}
