﻿using Infotrack.Utilitarios.Clases.Comunes.Entidades;
using InterfacesComunes.DTO;
using InterfacesComunes.DTO.EntidadesConsulta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesComunes.Acciones.Entidades
{
    public interface INotaAccion
    {

        Respuesta<INotaDTO> GuardarNota(INotaDTO nota);
        Respuesta<INotaDTO> GuardarListaNota(List<INotaDTO> listaNota);
        Respuesta<INotaDTO> EditarNota(INotaDTO nota);
        Respuesta<INotaDTO> EditarNotaPorFiltro(INotaDTO nota, params string[] propiedades);
        Respuesta<INotaDTO> ConsultarListaNotas();
        Respuesta<INotaDTO> ConsultarListaNotasLLave(INotaDTO nota);
        Respuesta<INotaDTO> ConsultarListaNotasPorFiltro(INotaDTO nota, Expression<Func<INotaDTO, bool>> filtro);
        Respuesta<INotaDTO> EliminarNota(INotaDTO nota);
        Respuesta<IConsultaNotasDTO> ConsultarReporteNotas(IConsultaNotasDTO filtro);
  

    }
}
