﻿namespace InterfacesComunes.DTO.EntidadesConsulta
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface IConsultaNotasDTO
    {
        int idAlumno { get; set; }
        int idNota { get; set; }
        int idCurso { get; set; }
        int idMateria { get; set; }
        int idCursoMateria { get; set; }
        string Nombre { get; set; }
        string Curso { get; set; }
        string Materia { get; set; }
        decimal? Nota { get; set; }
    }
}
