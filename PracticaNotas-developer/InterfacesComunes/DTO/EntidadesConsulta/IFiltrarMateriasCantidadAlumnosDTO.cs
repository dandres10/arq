﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesComunes.DTO.EntidadesConsulta
{
    public interface IFiltrarMateriasCantidadAlumnosDTO
    {
        string Materia { get; set; }
        int CantidadEstudiantes { get; set; }
    }
}
