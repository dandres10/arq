﻿namespace InterfacesComunes.DTO
{
    public interface IAlumnoDTO
    {
        int IdAlumno { get; set; }
        string Nombre { get; set; }
        string Apellido { get; set; }
        string Correo { get; set; }
        short Estado { get; set; }
    }
}