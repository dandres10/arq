﻿namespace InterfacesComunes.DTO
{
    using System;

    public interface ICursoDTO
    {
        int IdCurso { get; set; }
        string Nombre { get; set; }
        short EstadoCurso { get; set; }
        DateTime FechaInicio { get; set; }
        DateTime FechaFinal { get; set; }
    }
}