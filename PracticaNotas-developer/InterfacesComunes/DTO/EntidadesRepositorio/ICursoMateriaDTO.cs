﻿namespace InterfacesComunes.DTO
{
    public interface ICursoMateriaDTO
    {
        int IdCursoMateria { get; set; }
        short Estado { get; set; }
        int IdMateria { get; set; }
        int IdCurso { get; set; }
    }
}