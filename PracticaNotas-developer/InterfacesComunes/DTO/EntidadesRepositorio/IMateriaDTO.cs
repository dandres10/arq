﻿namespace InterfacesComunes.DTO
{
    public interface IMateriaDTO
    {
        int IdMateria { get; set; }
        string Nombre { get; set; }
        short EstadoMateria { get; set; }
    }
}