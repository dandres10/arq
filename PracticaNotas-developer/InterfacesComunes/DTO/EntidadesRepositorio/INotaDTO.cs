﻿namespace InterfacesComunes.DTO
{
    public interface INotaDTO
    {
        int IdNota { get; set; }
        int? IdCursoMateria { get; set; }
        int? IdAlumno { get; set; }
        decimal? Nota1 { get; set; }
    }
}