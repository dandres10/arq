﻿namespace Negocio.Clases.BL.AdministracionAlumno
{
    using Infotrack.Transaccional.EF.Clases;
    using Infotrack.Utilitarios.Clases.Comunes;
    using Infotrack.Utilitarios.Clases.Comunes.Entidades;
    using Infotrack.Utilitarios.Clases.Excepciones;
    using Infotrack.Utilitarios.Clases.Mapeador.Extensiones;
    using Infotrack.Utilitarios.Constantes;
    using InterfacesComunes.Acciones.Entidades;
    using InterfacesComunes.App_LocalResources;
    using InterfacesComunes.DTO;
    using Negocio.Clases.BO.EntidadesRepositorio;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;
    public class AlumnoBL : AccesoComunBL, IAlumnoAccion
    {
        private Lazy<IAlumnoAccion> RepositorioAlumno;
        private Respuesta<IAlumnoDTO> RespuestaAlumno;

        public AlumnoBL( Lazy<IAlumnoAccion> repositorioAlumno)
        {
            this.RespuestaAlumno = new Respuesta<IAlumnoDTO>();
            RepositorioAlumno = repositorioAlumno;
        }

        public Respuesta<IAlumnoDTO> ConsultarListaAlumnos()
        {
            return this.EjecutarTransaccionBD<Respuesta<IAlumnoDTO>, AlumnoBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioAlumno.Value.ConsultarListaAlumnos();
            });
        }

        public Respuesta<IAlumnoDTO> ConsultarListaAlumnosLLave(IAlumnoDTO alumno)
        {
            return this.EjecutarTransaccionBD<Respuesta<IAlumnoDTO>, AlumnoBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioAlumno.Value.ConsultarListaAlumnosLLave(alumno);
            });
        }

        public Respuesta<IAlumnoDTO> ConsultarListaAlumnosPorFiltro(IAlumnoDTO alumno, Expression<Func<IAlumnoDTO, bool>> filtro)
        {
            return this.EjecutarTransaccionBD<Respuesta<IAlumnoDTO>, AlumnoBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioAlumno.Value.ConsultarListaAlumnosPorFiltro(alumno, filtro);
            });
        }

        public Respuesta<IAlumnoDTO> EditarAlumno(IAlumnoDTO alumno)
        {
            return this.EjecutarTransaccionBD<Respuesta<IAlumnoDTO>, AlumnoBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                var resultado = RepositorioAlumno.Value.EditarAlumno(alumno);

                if (resultado.Mensajes.Any())
                {
                    return new Respuesta<IAlumnoDTO>
                    {
                        Mensajes = new List<string>() { MensajesComunes.MensajeCreacionEdicionExitosa },
                        Resultado = true,
                    };
                }
                else
                {
                    return resultado;
                }
            });
        }

        public Respuesta<IAlumnoDTO> EditarAlumnoPorFiltro(IAlumnoDTO alumno, params string[] propiedades)
        {
            return this.EjecutarTransaccionBD<Respuesta<IAlumnoDTO>, AlumnoBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioAlumno.Value.EditarAlumnoPorFiltro(alumno, propiedades);
            });
        }

        public Respuesta<IAlumnoDTO> EliminarAlumno(IAlumnoDTO alumno)
        {
            return this.EjecutarTransaccionBD<Respuesta<IAlumnoDTO>, AlumnoBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                var respuesta = RepositorioAlumno.Value.EliminarAlumno(alumno);

                if (respuesta.Mensajes.Any())
                {
                    return new Respuesta<IAlumnoDTO>
                    {
                        Mensajes = new List<string>() { MensajesComunes.MensajeCreacionEdicionExitosa },
                        Resultado = true,

                    };
                }
                else
                {
                    return respuesta;
                }
            });
        }

        private void ValidarAlumno(IAlumnoDTO alumno)
        {
            AlumnoBO alumnoBO = Mapeador.MapearEntidadDTO(alumno, new AlumnoBO());
            List<string> listaMensajesValidacion = alumnoBO.ValidarEntidadPorAtributos();

            if (listaMensajesValidacion.Any())
            {
                throw new ExcepcionControladaExeption("Validar Alumno", string.Join(ConstantesComunes.COMA, listaMensajesValidacion), false);
            }
        }

        public Respuesta<IAlumnoDTO> GuardarAlumno(IAlumnoDTO alumno)
        {
            return this.EjecutarTransaccionBD<Respuesta<IAlumnoDTO>, AlumnoBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                ValidarAlumno(alumno);
                var resultado = RepositorioAlumno.Value.GuardarAlumno(alumno);
                if (resultado.Mensajes.Any())
                {
                    return new Respuesta<IAlumnoDTO>
                    {
                        Mensajes = new List<string>() { MensajesComunes.MensajeCreacionEdicionExitosa },
                        Resultado = true
                    };
                }
                else
                {
                    return resultado;
                }
            });
        }

        public Respuesta<IAlumnoDTO> GuardarListaAlumno(List<IAlumnoDTO> listaAlumno)
        {
            return this.EjecutarTransaccionBD<Respuesta<IAlumnoDTO>, AlumnoBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioAlumno.Value.GuardarListaAlumno(listaAlumno);
            });
        }
    }
}
