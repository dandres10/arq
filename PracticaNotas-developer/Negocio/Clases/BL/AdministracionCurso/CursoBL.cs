﻿namespace Negocio.Clases.BL.AdministracionCurso
{
    using Infotrack.Transaccional.EF.Clases;
    using Infotrack.Utilitarios.Clases.Comunes;
    using Infotrack.Utilitarios.Clases.Comunes.Entidades;
    using Infotrack.Utilitarios.Clases.Excepciones;
    using Infotrack.Utilitarios.Clases.Mapeador.Extensiones;
    using Infotrack.Utilitarios.Constantes;
    using InterfacesComunes.Acciones.Entidades;
    using InterfacesComunes.App_LocalResources;
    using InterfacesComunes.DTO;
    using Negocio.Clases.BO.EntidadesRepositorio;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;
    public class CursoBL : AccesoComunBL, ICursoAccion
    {
        private Lazy<ICursoAccion> RepositorioCurso;
        private Respuesta<ICursoDTO> RespuestaCurso;

        public CursoBL(Lazy<ICursoAccion> repositorioCurso)
        {
            this.RespuestaCurso = new Respuesta<ICursoDTO>();
            RepositorioCurso = repositorioCurso;
        }

        public Respuesta<ICursoDTO> ConsultarListaCursos()
        {
            return this.EjecutarTransaccionBD<Respuesta<ICursoDTO>, CursoBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioCurso.Value.ConsultarListaCursos();
            });
        }

        public Respuesta<ICursoDTO> ConsultarListaCursosLLave(ICursoDTO curso)
        {
            return this.EjecutarTransaccionBD<Respuesta<ICursoDTO>, CursoBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioCurso.Value.ConsultarListaCursosLLave(curso);
            });
        }

        public Respuesta<ICursoDTO> ConsultarListaCursosPorFiltro(ICursoDTO curso, Expression<Func<ICursoDTO, bool>> filtro)
        {
            return this.EjecutarTransaccionBD<Respuesta<ICursoDTO>, CursoBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioCurso.Value.ConsultarListaCursosPorFiltro(curso, filtro);
            });
        }

        public Respuesta<ICursoDTO> EditarCurso(ICursoDTO curso)
        {
            return this.EjecutarTransaccionBD<Respuesta<ICursoDTO>, CursoBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                var resultado = RepositorioCurso.Value.EditarCurso(curso);

                if (resultado.Mensajes.Any())
                {
                    return new Respuesta<ICursoDTO>
                    {
                        Mensajes = new List<string>() { MensajesComunes.MensajeCreacionEdicionExitosa },
                        Resultado = true,
                    };
                }
                else
                {
                    return resultado;
                }
            });
        }

        public Respuesta<ICursoDTO> EditarCursoPorFiltro(ICursoDTO curso, params string[] propiedades)
        {
            return this.EjecutarTransaccionBD<Respuesta<ICursoDTO>, CursoBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioCurso.Value.EditarCursoPorFiltro(curso, propiedades);
            });
        }

        public Respuesta<ICursoDTO> EliminarCurso(ICursoDTO curso)
        {
            return this.EjecutarTransaccionBD<Respuesta<ICursoDTO>, CursoBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                var respuesta = RepositorioCurso.Value.EliminarCurso(curso);

                if (respuesta.Mensajes.Any())
                {
                    return new Respuesta<ICursoDTO>
                    {
                        Mensajes = new List<string>() { MensajesComunes.MensajeCreacionEdicionExitosa },
                        Resultado = true,

                    };
                }
                else
                {
                    return respuesta;
                }
            });
        }

        private void ValidarCurso(ICursoDTO curso)
        {
            CursoBO cursoBO = Mapeador.MapearEntidadDTO(curso, new CursoBO());
            List<string> listaMensajesValidacion = cursoBO.ValidarEntidadPorAtributos();

            if (listaMensajesValidacion.Any())
            {
                throw new ExcepcionControladaExeption("Validar Curso", string.Join(ConstantesComunes.COMA, listaMensajesValidacion), false);
            }
        }

        public Respuesta<ICursoDTO> GuardarCurso(ICursoDTO curso)
        {
            return this.EjecutarTransaccionBD<Respuesta<ICursoDTO>, CursoBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                ValidarCurso(curso);
                var resultado = RepositorioCurso.Value.GuardarCurso(curso);
                if (resultado.Mensajes.Any())
                {
                    return new Respuesta<ICursoDTO>
                    {
                        Mensajes = new List<string>() { MensajesComunes.MensajeCreacionEdicionExitosa },
                        Resultado = true
                    };
                }
                else
                {
                    return resultado;
                }
            });
        }

        public Respuesta<ICursoDTO> GuardarListaCurso(List<ICursoDTO> listaCurso)
        {
            return this.EjecutarTransaccionBD<Respuesta<ICursoDTO>, CursoBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioCurso.Value.GuardarListaCurso(listaCurso);
            });
        }
    }
}