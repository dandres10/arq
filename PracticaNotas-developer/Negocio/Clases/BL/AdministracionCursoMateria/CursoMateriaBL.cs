﻿namespace Negocio.Clases.BL.AdministracionCursoMateria
{
    using Infotrack.Transaccional.EF.Clases;
    using Infotrack.Utilitarios.Clases.Comunes;
    using Infotrack.Utilitarios.Clases.Comunes.Entidades;
    using Infotrack.Utilitarios.Clases.Excepciones;
    using Infotrack.Utilitarios.Clases.Mapeador.Extensiones;
    using Infotrack.Utilitarios.Constantes;
    using InterfacesComunes.Acciones.Entidades;
    using InterfacesComunes.App_LocalResources;
    using InterfacesComunes.DTO;
    using Negocio.Clases.BO.EntidadesRepositorio;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;
    public class CursoMateriaBL : AccesoComunBL, ICursoMateriaAccion
    {
        private Lazy<ICursoMateriaAccion> RepositorioCursoMateria;
        private Respuesta<ICursoMateriaDTO> RespuestaCursoMateria;

        public CursoMateriaBL(Lazy<ICursoMateriaAccion> repositorioCursoMateria)
        {
            this.RespuestaCursoMateria = new Respuesta<ICursoMateriaDTO>();
            RepositorioCursoMateria = repositorioCursoMateria;
        }

        public Respuesta<ICursoMateriaDTO> ConsultarListaCursoMaterias()
        {
            return this.EjecutarTransaccionBD<Respuesta<ICursoMateriaDTO>, CursoMateriaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioCursoMateria.Value.ConsultarListaCursoMaterias();
            });
        }

        public Respuesta<ICursoMateriaDTO> ConsultarListaCursoMateriasLLave(ICursoMateriaDTO cursoMateria)
        {
            return this.EjecutarTransaccionBD<Respuesta<ICursoMateriaDTO>, CursoMateriaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioCursoMateria.Value.ConsultarListaCursoMateriasLLave(cursoMateria);
            });
        }

        public Respuesta<ICursoMateriaDTO> ConsultarListaCursoMateriasPorFiltro(ICursoMateriaDTO cursoMateria, Expression<Func<ICursoMateriaDTO, bool>> filtro)
        {
            return this.EjecutarTransaccionBD<Respuesta<ICursoMateriaDTO>, CursoMateriaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioCursoMateria.Value.ConsultarListaCursoMateriasPorFiltro(cursoMateria, filtro);
            });
        }

        public Respuesta<ICursoMateriaDTO> EditarCursoMateria(ICursoMateriaDTO cursoMateria)
        {
            return this.EjecutarTransaccionBD<Respuesta<ICursoMateriaDTO>, CursoMateriaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                var resultado = RepositorioCursoMateria.Value.EditarCursoMateria(cursoMateria);

                if (resultado.Mensajes.Any())
                {
                    return new Respuesta<ICursoMateriaDTO>
                    {
                        Mensajes = new List<string>() { MensajesComunes.MensajeCreacionEdicionExitosa },
                        Resultado = true,
                    };
                }
                else
                {
                    return resultado;
                }
            });
        }

        public Respuesta<ICursoMateriaDTO> EditarCursoMateriaPorFiltro(ICursoMateriaDTO cursoMateria, params string[] propiedades)
        {
            return this.EjecutarTransaccionBD<Respuesta<ICursoMateriaDTO>, CursoMateriaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioCursoMateria.Value.EditarCursoMateriaPorFiltro(cursoMateria, propiedades);
            });
        }

        public Respuesta<ICursoMateriaDTO> EliminarCursoMateria(ICursoMateriaDTO cursoMateria)
        {
            return this.EjecutarTransaccionBD<Respuesta<ICursoMateriaDTO>, CursoMateriaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                var respuesta = RepositorioCursoMateria.Value.EliminarCursoMateria(cursoMateria);

                if (respuesta.Mensajes.Any())
                {
                    return new Respuesta<ICursoMateriaDTO>
                    {
                        Mensajes = new List<string>() { MensajesComunes.MensajeCreacionEdicionExitosa },
                        Resultado = true,

                    };
                }
                else
                {
                    return respuesta;
                }
            });
        }

        private void ValidarCursoMateria(ICursoMateriaDTO cursoMateria)
        {
            CursoMateriaBO cursoMateriaBO = Mapeador.MapearEntidadDTO(cursoMateria, new CursoMateriaBO());
            List<string> listaMensajesValidacion = cursoMateriaBO.ValidarEntidadPorAtributos();

            if (listaMensajesValidacion.Any())
            {
                throw new ExcepcionControladaExeption("Validar CursoMateria", string.Join(ConstantesComunes.COMA, listaMensajesValidacion), false);
            }
        }

        public Respuesta<ICursoMateriaDTO> GuardarCursoMateria(ICursoMateriaDTO cursoMateria)
        {
            return this.EjecutarTransaccionBD<Respuesta<ICursoMateriaDTO>, CursoMateriaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                ValidarCursoMateria(cursoMateria);
                var resultado = RepositorioCursoMateria.Value.GuardarCursoMateria(cursoMateria);
                if (resultado.Mensajes.Any())
                {
                    return new Respuesta<ICursoMateriaDTO>
                    {
                        Mensajes = new List<string>() { MensajesComunes.MensajeCreacionEdicionExitosa },
                        Resultado = true
                    };
                }
                else
                {
                    return resultado;
                }
            });
        }

        public Respuesta<ICursoMateriaDTO> GuardarListaCursoMateria(List<ICursoMateriaDTO> listaCursoMateria)
        {
            return this.EjecutarTransaccionBD<Respuesta<ICursoMateriaDTO>, CursoMateriaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioCursoMateria.Value.GuardarListaCursoMateria(listaCursoMateria);
            });
        }
    }
}
