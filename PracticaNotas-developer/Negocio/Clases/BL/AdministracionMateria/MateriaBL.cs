﻿namespace Negocio.Clases.BL.AdministracionMateria
{
    using Infotrack.Transaccional.EF.Clases;
    using Infotrack.Utilitarios.Clases.Comunes;
    using Infotrack.Utilitarios.Clases.Comunes.Entidades;
    using Infotrack.Utilitarios.Clases.Excepciones;
    using Infotrack.Utilitarios.Clases.Mapeador.Extensiones;
    using Infotrack.Utilitarios.Constantes;
    using InterfacesComunes.Acciones.Entidades;
    using InterfacesComunes.App_LocalResources;
    using InterfacesComunes.DTO;
    using InterfacesComunes.DTO.EntidadesConsulta;
    using Negocio.Clases.BO.EntidadesRepositorio;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
  
    public class MateriaBL : AccesoComunBL, IMateriaAccion
    {
        private Lazy<IMateriaAccion> RepositorioMateria;
        private Respuesta<IMateriaDTO> RespuestaMateria;

        public MateriaBL(Lazy<IMateriaAccion> repositorioMateria)
        {
            this.RespuestaMateria = new Respuesta<IMateriaDTO>();
            RepositorioMateria = repositorioMateria;
        }

        public Respuesta<IMateriaDTO> ConsultarListaMaterias()
        {
            return this.EjecutarTransaccionBD<Respuesta<IMateriaDTO>, MateriaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioMateria.Value.ConsultarListaMaterias();
            });
        }

        public Respuesta<IMateriaDTO> ConsultarListaMateriasLLave(IMateriaDTO materia)
        {
            return this.EjecutarTransaccionBD<Respuesta<IMateriaDTO>, MateriaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioMateria.Value.ConsultarListaMateriasLLave(materia);
            });
        }

        public Respuesta<IMateriaDTO> ConsultarListaMateriasPorFiltro(IMateriaDTO materia, Expression<Func<IMateriaDTO, bool>> filtro)
        {
            return this.EjecutarTransaccionBD<Respuesta<IMateriaDTO>, MateriaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioMateria.Value.ConsultarListaMateriasPorFiltro(materia, filtro);
            });
        }

        public Respuesta<IMateriaDTO> EditarMateria(IMateriaDTO materia)
        {
            return this.EjecutarTransaccionBD<Respuesta<IMateriaDTO>, MateriaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                var resultado = RepositorioMateria.Value.EditarMateria(materia);

                if (resultado.Mensajes.Any())
                {
                    return new Respuesta<IMateriaDTO>
                    {
                        Mensajes = new List<string>() { MensajesComunes.MensajeCreacionEdicionExitosa },
                        Resultado = true,
                    };
                }
                else
                {
                    return resultado;
                }
            });
        }

        public Respuesta<IMateriaDTO> EditarMateriaPorFiltro(IMateriaDTO materia, params string[] propiedades)
        {
            return this.EjecutarTransaccionBD<Respuesta<IMateriaDTO>, MateriaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioMateria.Value.EditarMateriaPorFiltro(materia, propiedades);
            });
        }

        public Respuesta<IMateriaDTO> EliminarMateria(IMateriaDTO materia)
        {
            return this.EjecutarTransaccionBD<Respuesta<IMateriaDTO>, MateriaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                var respuesta = RepositorioMateria.Value.EliminarMateria(materia);

                if (respuesta.Mensajes.Any())
                {
                    return new Respuesta<IMateriaDTO>
                    {
                        Mensajes = new List<string>() { MensajesComunes.MensajeCreacionEdicionExitosa },
                        Resultado = true,

                    };
                }
                else
                {
                    return respuesta;
                }
            });
        }

        private void ValidarMateria(IMateriaDTO materia)
        {
            MateriaBO materiaBO = Mapeador.MapearEntidadDTO(materia, new MateriaBO());
            List<string> listaMensajesValidacion = materiaBO.ValidarEntidadPorAtributos();

            if (listaMensajesValidacion.Any())
            {
                throw new ExcepcionControladaExeption("Validar Materia", string.Join(ConstantesComunes.COMA, listaMensajesValidacion), false);
            }
        }

        public Respuesta<IMateriaDTO> GuardarMateria(IMateriaDTO materia)
        {
            return this.EjecutarTransaccionBD<Respuesta<IMateriaDTO>, MateriaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                ValidarMateria(materia);
                var resultado = RepositorioMateria.Value.GuardarMateria(materia);
                if (resultado.Mensajes.Any())
                {
                    return resultado;

                }
                else
                {
                    return new Respuesta<IMateriaDTO>
                    {
                        Mensajes = new List<string>() { MensajesComunes.MensajeCreacionEdicionExitosa },
                        Resultado = true
                    };
                }
            });
        }

        public Respuesta<IMateriaDTO> GuardarListaMateria(List<IMateriaDTO> listaMateria)
        {
            return this.EjecutarTransaccionBD<Respuesta<IMateriaDTO>, MateriaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioMateria.Value.GuardarListaMateria(listaMateria);
            });
        }

        public Respuesta<IFiltrarMateriasCantidadAlumnosDTO> FiltrarMateriasCantidadAlumnos()
        {
            return this.EjecutarTransaccionBD<Respuesta<IFiltrarMateriasCantidadAlumnosDTO>, MateriaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioMateria.Value.FiltrarMateriasCantidadAlumnos();
            });
        }
    }
}
