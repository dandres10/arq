﻿namespace Negocio.Clases.BL.AdministracionNota
{
    using Infotrack.Transaccional.EF.Clases;
    using Infotrack.Utilitarios.Clases.Comunes;
    using Infotrack.Utilitarios.Clases.Comunes.Entidades;
    using Infotrack.Utilitarios.Clases.Excepciones;
    using Infotrack.Utilitarios.Clases.Mapeador.Extensiones;
    using Infotrack.Utilitarios.Constantes;
    using InterfacesComunes.Acciones.Entidades;
    using InterfacesComunes.App_LocalResources;
    using InterfacesComunes.DTO;
    using InterfacesComunes.DTO.EntidadesConsulta;
    using Negocio.Clases.BO.EntidadesRepositorio;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;
    public class NotaBL : AccesoComunBL, INotaAccion
    {
        private Lazy<INotaAccion> RepositorioNota;
        private Respuesta<INotaDTO> RespuestaNota;

        public NotaBL(Lazy<INotaAccion> repositorioNota)
        {
            this.RespuestaNota = new Respuesta<INotaDTO>();
            RepositorioNota = repositorioNota;
        }

        public Respuesta<INotaDTO> ConsultarListaNotas()
        {
            return this.EjecutarTransaccionBD<Respuesta<INotaDTO>, NotaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioNota.Value.ConsultarListaNotas();
            });
        }

        public Respuesta<INotaDTO> ConsultarListaNotasLLave(INotaDTO nota)
        {
            return this.EjecutarTransaccionBD<Respuesta<INotaDTO>, NotaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioNota.Value.ConsultarListaNotasLLave(nota);
            });
        }

        public Respuesta<INotaDTO> ConsultarListaNotasPorFiltro(INotaDTO nota, Expression<Func<INotaDTO, bool>> filtro)
        {
            return this.EjecutarTransaccionBD<Respuesta<INotaDTO>, NotaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioNota.Value.ConsultarListaNotasPorFiltro(nota, filtro);
            });
        }

        public Respuesta<INotaDTO> EditarNota(INotaDTO nota)
        {
            return this.EjecutarTransaccionBD<Respuesta<INotaDTO>, NotaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                var resultado = RepositorioNota.Value.EditarNota(nota);

                if (resultado.Mensajes.Any())
                {
                    return new Respuesta<INotaDTO>
                    {
                        Mensajes = new List<string>() { MensajesComunes.MensajeCreacionEdicionExitosa },
                        Resultado = true,
                    };
                }
                else
                {
                    return resultado;
                }
            });
        }

        public Respuesta<INotaDTO> EditarNotaPorFiltro(INotaDTO nota, params string[] propiedades)
        {
            return this.EjecutarTransaccionBD<Respuesta<INotaDTO>, NotaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioNota.Value.EditarNotaPorFiltro(nota, propiedades);
            });
        }

        public Respuesta<INotaDTO> EliminarNota(INotaDTO nota)
        {
            return this.EjecutarTransaccionBD<Respuesta<INotaDTO>, NotaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                var respuesta = RepositorioNota.Value.EliminarNota(nota);

                if (respuesta.Mensajes.Any())
                {
                    return new Respuesta<INotaDTO>
                    {
                        Mensajes = new List<string>() { MensajesComunes.MensajeCreacionEdicionExitosa },
                        Resultado = true,

                    };
                }
                else
                {
                    return respuesta;
                }
            });
        }

        private void ValidarNota(INotaDTO nota)
        {
            NotaBO notaBO = Mapeador.MapearEntidadDTO(nota, new NotaBO());
            List<string> listaMensajesValidacion = notaBO.ValidarEntidadPorAtributos();

            if (listaMensajesValidacion.Any())
            {
                throw new ExcepcionControladaExeption("Validar Nota", string.Join(ConstantesComunes.COMA, listaMensajesValidacion), false);
            }
        }

        public Respuesta<INotaDTO> GuardarNota(INotaDTO nota)
        {
            return this.EjecutarTransaccionBD<Respuesta<INotaDTO>, NotaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                ValidarNota(nota);
                var resultado = RepositorioNota.Value.GuardarNota(nota);
                if (resultado.Mensajes.Any())
                {
                    return new Respuesta<INotaDTO>
                    {
                        Mensajes = new List<string>() { MensajesComunes.MensajeCreacionEdicionExitosa },
                        Resultado = true
                    };
                }
                else
                {
                    return resultado;
                }
            });
        }

        public Respuesta<INotaDTO> GuardarListaNota(List<INotaDTO> listaNota)
        {
            return this.EjecutarTransaccionBD<Respuesta<INotaDTO>, NotaBL>(System.Transactions.IsolationLevel.ReadUncommitted, () =>
            {
                return RepositorioNota.Value.GuardarListaNota(listaNota);
            });
        }

        public Respuesta<IConsultaNotasDTO> ConsultarReporteNotas(IConsultaNotasDTO filtro)
        {
       
                    return RepositorioNota.Value.ConsultarReporteNotas(filtro);
                


            
        }

       
    }
}
