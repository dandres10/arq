﻿using InterfacesComunes.DTO.EntidadesConsulta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Clases.BO.EntidadesConsulta
{
    public class FiltraMateriaCantidadAlumnosBO : IFiltrarMateriasCantidadAlumnosDTO
    {
        public string Materia { get; set; }
        public int CantidadEstudiantes { get; set; }
    }
}
