﻿namespace Negocio.Clases.BO.EntidadesRepositorio
{
    using InterfacesComunes.DTO;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AlumnoBO : IAlumnoDTO
    {
        public int IdAlumno { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Correo { get; set; }
        public short Estado { get; set; }
    }
}
