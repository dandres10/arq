﻿namespace Negocio.Clases.BO.EntidadesRepositorio
{
    using InterfacesComunes.DTO;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class CursoBO : ICursoDTO
    {
        public int IdCurso { get; set; }
        public string Nombre { get; set; }
        public short EstadoCurso { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFinal { get; set; }
    }
}
