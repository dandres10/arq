﻿namespace Negocio.Clases.BO.EntidadesRepositorio
{
    using InterfacesComunes.DTO;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class CursoMateriaBO : ICursoMateriaDTO
    {
        public int IdCursoMateria { get; set; }
        public short Estado { get; set; }
        public int IdMateria { get; set; }
        public int IdCurso { get; set; }
    }
}
