﻿namespace Negocio.Clases.BO.EntidadesRepositorio
{
    using InterfacesComunes.DTO;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    public class MateriaBO : IMateriaDTO
    {
        public int IdMateria { get; set; }
        public string Nombre { get; set; }
        public short EstadoMateria { get; set; }
    }
}
