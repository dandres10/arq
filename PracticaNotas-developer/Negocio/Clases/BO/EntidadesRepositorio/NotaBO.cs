﻿namespace Negocio.Clases.BO.EntidadesRepositorio
{
    using InterfacesComunes.DTO;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class NotaBO : INotaDTO
    {
        public int IdNota { get; set; }
        public int? IdCursoMateria { get; set; }
        public int? IdAlumno { get; set; }
        public decimal? Nota1 { get; set; }
    }
}
