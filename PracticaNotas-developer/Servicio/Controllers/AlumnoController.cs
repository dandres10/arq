﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using Datos.Clases.DAL;
using Infotrack.Utilitarios.API.Clases.Ejecucion;
using Infotrack.Utilitarios.Clases.Comunes.Entidades;
using Infotrack.Utilitarios.Clases.Mapeador.Extensiones;
using InterfacesComunes.Acciones.Entidades;
using InterfacesComunes.DTO;
using Negocio.Clases.BL.AdministracionAlumno;
using Servicio.Models;

namespace Servicio.Controllers
{
    [RoutePrefix("api/Alumno")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AlumnoController : AccesoComunAPI
    {
        
        private Lazy<AlumnoBL> NegocioAlumno;

        public AlumnoController()
        {
            NegocioAlumno = new Lazy<AlumnoBL>(() => new AlumnoBL(new Lazy<IAlumnoAccion>(() => new AlumnoDAL())));
        }
        // GET: api/Alumnos


        [HttpGet]
        [Route("ConsultarListaAlumno")]
        public Respuesta<Alumno> ConsultarListaAlumno()
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Alumno>>(NegocioAlumno.Value.ConsultarListaAlumnos());
        }
        [HttpGet]
        [Route("ConsultarListaAlumnoLLave")]
        public Respuesta<Alumno> ConsultarListaAlumnosLLave(Alumno alumno)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Alumno>>(NegocioAlumno.Value.ConsultarListaAlumnosLLave(alumno));
        }

        [HttpGet]
        [Route("ConsultarListaAlumnoPorFiltro")]
        public Respuesta<Alumno> ConsultarListaAlumnosPorFiltro(IAlumnoDTO alumno, Expression<Func<IAlumnoDTO, bool>> filtro)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Alumno>>(NegocioAlumno.Value.ConsultarListaAlumnosPorFiltro(alumno,filtro));
        }

        [HttpPut]
        [Route("EditarAlumno")]
        public Respuesta<Alumno> EditarAlumno(Alumno alumno)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Alumno>>(NegocioAlumno.Value.EditarAlumno(alumno));
        }


        [HttpPut]
        [Route("EditarAlumnoPorFiltro")]
        public Respuesta<Alumno> EditarAlumnoPorFiltro(Alumno alumno, params string[] propiedades)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Alumno>>(NegocioAlumno.Value.EditarAlumnoPorFiltro(alumno,propiedades));
        }

        [HttpDelete]
        [Route("EliminarAlumno")]
        public Respuesta<Alumno> EliminarAlumno(Alumno alumno)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Alumno>>(NegocioAlumno.Value.EliminarAlumno(alumno));
        }

        [HttpPost]
        [Route("AgregarAlumno")]

        public Respuesta<Alumno> GuardarAlumno(Alumno alumno)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Alumno>>(NegocioAlumno.Value.GuardarAlumno(alumno));
        }
    }
}