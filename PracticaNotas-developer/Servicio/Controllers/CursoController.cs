﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using Datos.Clases.DAL;
using Infotrack.Utilitarios.API.Clases.Ejecucion;
using Infotrack.Utilitarios.Clases.Comunes.Entidades;
using Infotrack.Utilitarios.Clases.Mapeador.Extensiones;
using InterfacesComunes.Acciones.Entidades;
using InterfacesComunes.DTO;
using Negocio.Clases.BL.AdministracionCurso;
using Servicio.Models.ModelosRepositorio;

namespace Servicio.Controllers
{
    [RoutePrefix("api/Curso")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CursoController : AccesoComunAPI
    {
        private Lazy<CursoBL> NegocioCurso;

        public CursoController()
        {
            NegocioCurso = new Lazy<CursoBL>(() => new CursoBL(new Lazy<ICursoAccion>(() => new CursoDAL())));
        }
        // GET: api/Cursos


        [HttpGet]
        [Route("ConsultarListaCurso")]
        public Respuesta<Curso> ConsultarListaCurso()
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Curso>>(NegocioCurso.Value.ConsultarListaCursos());
        }

        [HttpGet]
        [Route("ConsultarListaCursoLLave")]
        public Respuesta<Curso> ConsultarListaCursosLLave(Curso curso)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Curso>>(NegocioCurso.Value.ConsultarListaCursosLLave(curso));
        }

        [HttpGet]
        [Route("ConsultarListaCursoPorFiltro")]
        public Respuesta<Curso> ConsultarListaCursosPorFiltro(ICursoDTO curso, Expression<Func<ICursoDTO, bool>> filtro)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Curso>>(NegocioCurso.Value.ConsultarListaCursosPorFiltro(curso, filtro));
        }

        [HttpPut]
        [Route("EditarCurso")]
        public Respuesta<Curso> EditarCurso(Curso curso)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Curso>>(NegocioCurso.Value.EditarCurso(curso));
        }


        [HttpPut]
        [Route("EditarCursoPorFiltro")]
        public Respuesta<Curso> EditarCursoPorFiltro(Curso curso, params string[] propiedades)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Curso>>(NegocioCurso.Value.EditarCursoPorFiltro(curso, propiedades));
        }

        [HttpDelete]
        [Route("EliminarCurso")]
        public Respuesta<Curso> EliminarCurso(Curso curso)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Curso>>(NegocioCurso.Value.EliminarCurso(curso));
        }

        [HttpPost]
        [Route("AgregarCurso")]

        public Respuesta<Curso> GuardarCurso(Curso curso)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Curso>>(NegocioCurso.Value.GuardarCurso(curso));
        }

    }
}