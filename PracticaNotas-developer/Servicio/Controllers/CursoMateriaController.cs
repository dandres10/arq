﻿namespace Servicio.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.Cors;
    using System.Web.Http.Description;
    using Datos.Clases.DAL;
    using Infotrack.Utilitarios.API.Clases.Ejecucion;
    using Infotrack.Utilitarios.Clases.Comunes.Entidades;
    using Infotrack.Utilitarios.Clases.Mapeador.Extensiones;
    using InterfacesComunes.Acciones.Entidades;
    using InterfacesComunes.DTO;
    using Negocio.Clases.BL.AdministracionCursoMateria;
    using Servicio.Models;

    [RoutePrefix("api/cursoMateria")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CursoMateriaController : AccesoComunAPI
    {

        private Lazy<CursoMateriaBL> NegocioCursoMateria;

        public CursoMateriaController()
        {
            NegocioCursoMateria = new Lazy<CursoMateriaBL>(() => new CursoMateriaBL(new Lazy<ICursoMateriaAccion>(() => new CursoMateriaDAL())));
        }
        // GET: api/CursoMateria


        [HttpGet]
        [Route("ConsultarCursoMateria")]
        public Respuesta<CursoMateria> ConsultarListaCursoMaterias()
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<CursoMateria>>(NegocioCursoMateria.Value.ConsultarListaCursoMaterias());
        }

        [HttpGet]
        [Route("ConsultarListaCursoMateriasLLave")]
        public Respuesta<CursoMateria> ConsultarListaCursoMateriasLLave(CursoMateria cursoMateria)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<CursoMateria>>(NegocioCursoMateria.Value.ConsultarListaCursoMateriasLLave(cursoMateria));
        }

        [HttpGet]
        [Route("ConsultarListaCursoMateriasPorFiltro")]
        public Respuesta<CursoMateria> ConsultarListaCursoMateriasPorFiltro(CursoMateria cursoMateria, Expression<Func<ICursoMateriaDTO, bool>> filtro)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<CursoMateria>>(NegocioCursoMateria.Value.ConsultarListaCursoMateriasPorFiltro(cursoMateria, filtro));
        }

        [HttpPut]
        [Route("EditarCursoMateria")]
        public Respuesta<CursoMateria> EditarCursoMateria(CursoMateria cursoMateria)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<CursoMateria>>(NegocioCursoMateria.Value.EditarCursoMateria(cursoMateria));
        }


        [HttpPut]
        [Route("EditarCursoMateriaPorFiltro")]
        public Respuesta<CursoMateria> EditarCursoMateriaPorFiltro(CursoMateria cursoMateria, params string[] propiedades)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<CursoMateria>>(NegocioCursoMateria.Value.EditarCursoMateriaPorFiltro(cursoMateria, propiedades));
        }

        [HttpDelete]
        [Route("EliminarCursoMateria")]
        public Respuesta<CursoMateria> EliminarCursoMateria(CursoMateria cursoMateria)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<CursoMateria>>(NegocioCursoMateria.Value.EliminarCursoMateria(cursoMateria));
        }

        [HttpPost]
        [Route("GuardarCursoMateria")]
        public Respuesta<CursoMateria> GuardarCursoMateria(ICursoMateriaDTO cursoMateria)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<CursoMateria>>(NegocioCursoMateria.Value.GuardarCursoMateria(cursoMateria));
        }



    }
}