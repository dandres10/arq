﻿namespace Servicio.Controllers
{
    using System;
    using System.Web.Http;
    using System.Web.Http.Cors;
    using Datos.Clases.DAL;
    using Infotrack.Utilitarios.API.Clases.Ejecucion;
    using Infotrack.Utilitarios.Clases.Comunes.Entidades;
    using Infotrack.Utilitarios.Clases.Mapeador.Extensiones;
    using InterfacesComunes.Acciones.Entidades;
    using Negocio.Clases.BL.AdministracionMateria;
    using Servicio.Models;

    [RoutePrefix("api/Materia")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class MateriaController : AccesoComunAPI
    {
        private Lazy<MateriaBL> NegocioMateria;

        public MateriaController()
        {
            NegocioMateria = new Lazy<MateriaBL>(() => new MateriaBL(new Lazy<IMateriaAccion>(() => new MateriaDAL())));
        }

        [HttpGet]
        [Route("ConsultarListaMateria")]
        public Respuesta<Materia> ConsultarListaMateria()
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Materia>>(NegocioMateria.Value.ConsultarListaMaterias());
        }

        [HttpGet]
        [Route("ConsultarMateriaLlave")]
        public Respuesta<Materia> ConsultarMateriaLlaveOperacion(Materia materia)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Materia>>(NegocioMateria.Value.ConsultarListaMateriasLLave(materia));
        }

        [HttpPost]
        [Route("GuardarMateria")]
        public Respuesta<Materia> GuardarMateriaOperacion(Materia materia)
        {
            return this.EjecutarTransaccionAPI<Respuesta<Materia>, MateriaController>(() =>
            {
                return Mapeador.MapearObjetoPorJson<Respuesta<Materia>>(NegocioMateria.Value.GuardarMateria(materia));
            });

        }

        [HttpPut]
        [Route("EditarMateria")]
        public Respuesta<Materia> EditarMateriaOperacion(Materia materia)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Materia>>(NegocioMateria.Value.EditarMateria(materia));
        }

        [HttpDelete]
        [Route("EliminarMateria")]
        public Respuesta<Materia> EliminarMateriaOperacion(Materia materia)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Materia>>(NegocioMateria.Value.EliminarMateria(materia));
        }
        [HttpGet]
        [Route("filtrarMateriasCantidadAlumnos")]
        public Respuesta<Materia> FiltrarMateriasCantidadAlumnos()
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Materia>>(NegocioMateria.Value.FiltrarMateriasCantidadAlumnos());
        }
    }
}