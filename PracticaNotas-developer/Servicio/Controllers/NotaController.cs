﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using Datos.Clases.DAL;
using Datos.Contexto;
using Infotrack.Utilitarios.API.Clases.Ejecucion;
using Infotrack.Utilitarios.Clases.Comunes.Entidades;
using Infotrack.Utilitarios.Clases.Mapeador.Extensiones;
using InterfacesComunes.Acciones.Entidades;
using InterfacesComunes.DTO.EntidadesConsulta;
using Negocio.Clases.BL.AdministracionNota;
using Servicio.Models;
using Servicio.Models.ModelosConsulta;
using Nota = Servicio.Models.Nota;

namespace Servicio.Controllers
{
    [RoutePrefix("api/Nota")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class NotaController : AccesoComunAPI
    {

        private Lazy<NotaBL> NegocioNota;

        public NotaController()
        {
            NegocioNota = new Lazy<NotaBL>(() => new NotaBL(new Lazy<INotaAccion>(() => new NotaDAL())));
        }
        // GET: api/Notas

        [HttpGet]
        [Route("ConsultarNotas")]
        public Respuesta<Nota> ConsultarListaNotas()
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Nota>>(NegocioNota.Value.ConsultarListaNotas());
        }

        [HttpPost]
        [Route("ConsultarReporteNota")]
        public Respuesta<ConsultaNotas> ConsultarReporteNotas(ConsultaNotasConcatenacion filtro)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<ConsultaNotas>>(NegocioNota.Value.ConsultarReporteNotas(filtro));
        }

        [HttpPost]
        [Route("InsertarNota")]
        public Respuesta<Nota> GuardarNotaOperacion(Nota nota)
        {
            return this.EjecutarTransaccionAPI<Respuesta<Nota>, NotaController>(() =>
            {
                return Mapeador.MapearObjetoPorJson<Respuesta<Nota>>(NegocioNota.Value.GuardarNota(nota));
            });

        }

        [HttpGet]
        [Route("ConsultarNotaLlave")]
        public Respuesta<Nota> ConsultarNotaLlaveOperacion(Nota nota)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Nota>>(NegocioNota.Value.ConsultarListaNotasLLave(nota));
        }

        [HttpPut]
        [Route("EditarNota")]
        public Respuesta<Nota> EditarNotaOperacion(Nota nota)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Nota>>(NegocioNota.Value.EditarNota(nota));
        }

        [HttpDelete]
        [Route("EliminarNota")]
        public Respuesta<Nota> EliminarNotaOperacion(Nota nota)
        {
            return Mapeador.MapearObjetoPorJson<Respuesta<Nota>>(NegocioNota.Value.EliminarNota(nota));
        }


    }
}