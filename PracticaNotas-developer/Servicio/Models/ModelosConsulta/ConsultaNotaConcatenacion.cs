﻿using InterfacesComunes.DTO.EntidadesConsulta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Servicio.Models.ModelosConsulta
{
    public class ConsultaNotasConcatenacion : IConsultaNotasDTO
    {
        public ConsultaNotasConcatenacion()
        {

        }
        public int idAlumno { get; set; }
        public int idNota { get; set; }
        public int idCurso { get; set; }
        public int idMateria { get; set; }
        public int idCursoMateria { get; set; }
        public string Nombre { get; set; }
        public string Curso { get; set; }
        public string Materia { get; set; }
        public decimal? Nota { get; set; }
        public string Apellido { get ; set ; }
    }
}