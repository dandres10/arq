﻿
namespace Servicio.Models
{
    using InterfacesComunes.DTO.EntidadesConsulta;

    public class ConsultaNotas : IConsultaNotasDTO
    {
        public ConsultaNotas()
        {

        }
        public int idAlumno { get ; set ; }
        public int idNota { get ; set ; }
        public int idCurso { get ; set ; }
        public int idMateria { get ; set ; }
        public int idCursoMateria { get ; set ; }
        public string Nombre { get ; set ; }
        public string Curso { get ; set ; }
        public string Materia { get ; set ; }
        public decimal? Nota { get ; set ; }
    }
}