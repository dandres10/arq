﻿


namespace Servicio.Models
{
    using InterfacesComunes.DTO;
    using System;
  

    public class Alumno : IAlumnoDTO
    {
        public Alumno()
        {

        }
        public int IdAlumno { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Correo { get; set; }
        public short Estado { get; set; }
    }
}