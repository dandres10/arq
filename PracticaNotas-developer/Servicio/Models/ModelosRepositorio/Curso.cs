﻿using InterfacesComunes.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Servicio.Models.ModelosRepositorio
{
    public class Curso : ICursoDTO
    {
        public Curso()
        {

        }
        public int IdCurso { get ; set ; }
        public string Nombre { get ; set ; }
        public short EstadoCurso { get ; set ; }
        public DateTime FechaInicio { get ; set ; }
        public DateTime FechaFinal { get ; set ; }
    }
}