﻿namespace Servicio.Models
{
    using InterfacesComunes.DTO;

    public class CursoMateria : ICursoMateriaDTO
    {
        public CursoMateria()
        {

        }
        public int IdCursoMateria { get ; set ; }
        public short Estado { get ; set ; }
        public int IdMateria { get ; set ; }
        public int IdCurso { get ; set ; }
    }
}