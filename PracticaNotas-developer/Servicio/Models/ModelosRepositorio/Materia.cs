﻿
namespace Servicio.Models
{
    using InterfacesComunes.DTO;

    public class Materia : IMateriaDTO
    {
        public Materia()
        {

        }
        public int IdMateria { get ; set ; }
        public string Nombre { get ; set ; }
        public short EstadoMateria { get ; set ; }
    }
}