﻿
namespace Servicio.Models
{
    using InterfacesComunes.DTO;
    public class Nota : INotaDTO
    {
        public Nota()
        {

        }
        public int IdNota { get; set; }
        public int? IdCursoMateria { get; set; }
        public int? IdAlumno { get; set; }
        public decimal? Nota1 { get; set; }
    }
}