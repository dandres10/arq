interface IAlumno {
    IdAlumno: number,
    Nombre: string,
    Apellido: string,
    Correo: string,
    Estado: number
}
export default IAlumno;