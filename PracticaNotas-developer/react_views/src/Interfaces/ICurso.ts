interface ICurso {
    IdCurso: number,
    Nombre: string,
    EstadoCurso: number,
    FechaInicio: string,
    FechaFinal: string
}
export default ICurso;