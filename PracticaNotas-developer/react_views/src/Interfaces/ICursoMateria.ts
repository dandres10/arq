interface ICursoMateria{
    IdCursoMateria: number,
    Estado: number,
    IdMateria: number,
    IdCurso: number
}
export default ICursoMateria;