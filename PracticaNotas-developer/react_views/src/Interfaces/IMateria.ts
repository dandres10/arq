interface IMateria{
    IdMateria: number,
    NombreMateria: string,
    EstadoMateria: number
}
export default IMateria;