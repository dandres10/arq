interface INota{
    idAlumno: number,
    idNota: number,
    idCurso: number,
    idMateria: number,
    idCursoMateria: number,
    Nombre: string,
    Curso: string,
    Materia: string,
    Nota: number
}
export default INota;