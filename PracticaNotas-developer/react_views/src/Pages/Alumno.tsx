import * as React from 'react';
import { Container } from '@material-ui/core';
import Navbar from '../components/Navbar';
import AlumnoTabla from '../components/Alumno/AlumnoTabla';

const Alumno: React.FC = () => {
    return(
        <div className="App">
            <Container maxWidth="lg">
                <Navbar/>
                <AlumnoTabla/>
            </Container>
        </div>

    );
}

export default Alumno;