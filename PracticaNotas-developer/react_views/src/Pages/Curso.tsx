import * as React from 'react';
import { Container } from '@material-ui/core';
import Navbar from '../components/Navbar';
import CursoTabla from '../components/Curso/CursoTabla';

const Curso: React.FC = () => {
    return(
        <div className="App">
            <Container maxWidth="lg">
                <Navbar/>
                <CursoTabla/>
            </Container>
        </div>

    );
}

export default Curso;