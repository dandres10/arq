import * as React from "react";
import '../App.css';
import { Container } from "@material-ui/core";
import Navbar from '../components/Navbar';

const Home: React.FC = () => {
  return (
    <div className="App">
      <Container maxWidth="lg">
        <Navbar/>
      </Container>
    </div>
  );
};

export default Home;
