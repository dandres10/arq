import * as React from 'react';
import { Container } from '@material-ui/core';
import Navbar from '../components/Navbar';
import NotaTabla from '../components/Notas/NotaTabla';

const Nota: React.FC = () => {
    return(
        <div className="App">
            <Container maxWidth="lg">
                <Navbar/>
                <NotaTabla/>
            </Container>
        </div>

    );
}

export default Nota;