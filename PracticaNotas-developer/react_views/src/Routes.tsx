import * as React from "react";
import { Switch, Route } from "react-router-dom";
import Home from "./Pages/Home";
import Materia from "./Pages/Materia";
import Alumno from "./Pages/Alumno";
import Curso from "./Pages/Curso";
import Nota from "./Pages/Nota";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/Materia" component={Materia} />
      <Route path="/Alumno" component={Alumno} />
      <Route path="/Curso" component={Curso} />
      <Route path="/Nota" component={Nota} />
    </Switch>
  );
};

export default Routes;
