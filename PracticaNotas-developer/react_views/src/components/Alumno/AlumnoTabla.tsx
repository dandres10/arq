import * as React from 'react';
import axios from 'axios';
import MaterialTable from 'material-table';
import IAlumno from '../../Interfaces/IAlumno';

const AlumnoTabla: React.FC = () => {

    const [alumnos,setAlumnos] = React.useState<IAlumno[]>([]);


    React.useEffect(()=>{
        axios.get('http://localhost:65469/api/Alumno/ConsultarListaAlumno')
            .then(res =>{
                const alumnoData = res.data.Entidades;
                setAlumnos(alumnoData);
            })
            .catch(error=>{
                console.log(error);
            })
    }, []);

        return(
            <div>
            <MaterialTable
                columns={[
                    {title:'Codigo', field:'IdAlumno'},
                    {title:'Nombre', field:'Nombre'},
                    {title:'Apellido', field:'Apellido'},
                    {title:'Correo', field:'Correo'},
                    {title:'Estado', field:'Estado'}
                ]}
                data ={alumnos}
                title = "Tabla Alumnos"
                />
            </div>
        );
}


export default AlumnoTabla;