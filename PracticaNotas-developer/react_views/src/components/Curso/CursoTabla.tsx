import * as React from 'react';
import axios from 'axios';
import MaterialTable from 'material-table';
import ICurso from '../../Interfaces/ICurso';

const CursoTabla: React.FC = () => {

    const [cursos,setCursos] = React.useState<ICurso[]>([]);


    React.useEffect(()=>{
        axios.get('http://localhost:65469/api/Curso/ConsultarListaCurso')
            .then(res =>{
                const cursoData = res.data.Entidades;
                setCursos(cursoData);
            })
            .catch(error=>{
                console.log(error);
            })
    }, []);

        return(
            <div>
            <MaterialTable
                columns={[
                    {title:'Codigo', field:'IdCurso'},
                    {title:'Nombre', field:'Nombre'},
                    {title:'Estado', field:'EstadoCurso'},
                    {title:'Inicio', field:'FechaInicio'},
                    {title:'Final', field:'FechaFinal'}
                ]}
                data ={cursos}
                title = "Tabla Cursos"
               
                />
            </div>
        );
}


export default CursoTabla;