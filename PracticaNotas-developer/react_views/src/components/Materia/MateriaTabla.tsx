import * as React from 'react';
import axios from 'axios';
import MaterialTable from 'material-table';
import IMateria from '../../Interfaces/IMateria';

const MateriaTable: React.FC = () => {

    const [materias,setMaterias] = React.useState<IMateria[]>([]);


    React.useEffect(()=>{
        axios.get('http://localhost:65469/api/Materia/ConsultarListaMateria')
            .then(res =>{
                const materiaData = res.data.Entidades;
                setMaterias(materiaData);
            })
            .catch(error=>{
                console.log(error);
            })
    }, []);

    async function handleAdd(){
        const objeto={Nombre:'Biologia',EstadoMateria:1};
        const {data:{Entidades}}=await axios.post('http://localhost:65469/api/Materia/GuardarMateria',objeto);
        const materiaData=[Entidades,...materias];
        setMaterias(materiaData);
    };


        return(
            <div>
            <MaterialTable
                columns={[
                    {title:'Codigo', field:'IdMateria'},
                    {title:'Nombre', field:'Nombre'},
                    {title:'Estado', field:'EstadoMateria'}
                ]}
                data ={materias}
                title = "Tabla Materias"
                />
                <button onClick={()=>handleAdd()}>Este el el post </button>
            </div>
        );
    
}


export default MateriaTable;