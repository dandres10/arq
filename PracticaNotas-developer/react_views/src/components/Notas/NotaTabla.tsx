import * as React from 'react';
import axios from 'axios';
import MaterialTable from 'material-table';
import INota from '../../Interfaces/INota';

const NotaTabla: React.FC = () => {

    const [notas,setNotas] = React.useState<INota[]>([]);


    React.useEffect(()=>{
        const busquedaDefecto = {};
        axios.post('http://localhost:65469/api/Nota/ConsultarReporteNota',busquedaDefecto)
            .then(res =>{
                const notaData = res.data.Entidades;
                setNotas(notaData);
            })
            .catch(error=>{
                console.log(error);
            })
    }, []);

        return(
            <div>
            <MaterialTable
                columns={[
                    {title:'Codigo', field:'idAlumno'},
                    {title:'Nombre', field:'Nombre'},
                    {title:'Curso', field:'Curso'},
                    {title:'Materia', field:'Materia'},
                    {title:'Nota', field:'Nota'}
                ]}
                data ={notas}
                title = "Tabla Notas"
                />
            </div>
        );
}


export default NotaTabla;